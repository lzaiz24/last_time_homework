import psutil
import platform


def get_cpu_times():
    cpu_times = psutil.cpu_times()
    os_info = platform.system()
    if os_info == "Windows":
        cpu_times_info = {
            "user": cpu_times.user,
            "system": cpu_times.system,
            "idle": cpu_times.idle,
            "interrupt": cpu_times.interrupt,
        }
    elif os_info == "Linux":
        cpu_times_info = {
            "user": cpu_times.user,
            "system": cpu_times.system,
            "idle": cpu_times.idle,
            "iowait": cpu_times.iowait,
        }

    return cpu_times_info


def get_cpu_conut():
    cpu_count_info = {
        "cpu_count": psutil.cpu_count()
    }
    return cpu_count_info


def get_cpu_count_logical():
    cpu_count_logical_info = {
        "cpu_count_logical": psutil.cpu_count(logical=False)
    }
    return cpu_count_logical_info


def get_cpu_loadavg():
    cpu_loadavg_info = {
        "cpu_loadavg": psutil.getloadavg()
    }
    return cpu_loadavg_info


def get_cpu_virtual_memory():
    cpu_virtual_memory_info = {
        "cpu_virtual_memory": psutil.virtual_memory()
    }
    return cpu_virtual_memory_info


def get_cpu_swap_memory():
    cpu_swap_memory = psutil.swap_memory()
    cpu_swap_memory_info = {
        "total": cpu_swap_memory.total,
        "used": cpu_swap_memory.used,
        "free": cpu_swap_memory.free,
        "percent": cpu_swap_memory.percent,
        "sin": cpu_swap_memory.sin,
        "sout": cpu_swap_memory.sout,
    }
    return cpu_swap_memory_info
