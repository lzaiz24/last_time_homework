from typing import Union
import tools
from fastapi import FastAPI

app = FastAPI()


@app.get("/items/{item_id}")
async def read_item(item_id: str, q: Union[str, None] = None):
    if q:
        return {"item_id": item_id, "q": q}
    return {"item_id": item_id}


@app.get("/cpu_times/")
def get_cpu_times_info():
    return tools.get_cpu_times()


@app.get("/cpu_count/")
def get_cpu_count_info():
    return tools.get_cpu_conut()


@app.get("/cpu_count_logical/")
def get_cpu_count_logical_info():
    return tools.get_cpu_count_logical()


@app.get("/cpu_loadavg/")
def get_cpu_loadavg_info():
    return tools.get_cpu_loadavg()


@app.get("/cpu_virtual_memory/")
def get_cpu_virtual_memory_info():
    return tools.get_cpu_virtual_memory()


@app.get("/cpu_swap_memory/")
def get_cpu_swap_memory_info():
    return tools.get_cpu_swap_memory()
